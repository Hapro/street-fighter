#include "PunchState.h"
#include "Character.h"
#include "CharacterAnimation.h"
#include "StandState.h"
#include "AnimationFrame.h"

PunchState::PunchState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pPunchAnimation);
	stateName = "PUNCH_STATE";
}

CharacterState* PunchState::update(float elapsed){
	//Exit if the animation has finished
	if (this->pAnimation->hasFinished()){
		pAnimation->reset();
		return new StandState(pParentCharacter);
	}

	return CharacterState::update(elapsed);
}

CharacterState* PunchState::punch(){
	//If this was a break frame, ignore it so we carry on
	if (this->pAnimation->getAtBreakFrame()){
		this->pAnimation->setIgnoreBreakFrame(true);
	}
	return this;
}