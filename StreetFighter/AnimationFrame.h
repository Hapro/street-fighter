#ifndef ANIMATION_FRAME
#define ANIMATION_FRAME

#include "SFML\Graphics.hpp"

class Character;

class DamagePoint{
private:
	int damageToDeal;
	sf::Vector2f force;
	sf::Vector2f localPosition;
	bool active;

public:
	DamagePoint();
	DamagePoint(int damageToDeal, sf::Vector2f force, sf::Vector2f localPosition);
	int getDamageToDeal();
	bool getActive();
	sf::Vector2f getForce();
	sf::Vector2f getLocalPosition();
	sf::Vector2f getFlippedLocalPosition();

};

class AnimationFrame{
private:
	sf::Sprite sprite;
	DamagePoint* pDamagePoint;
	bool breakFrame = false;
	sf::FloatRect collisionBounds;

public:
	~AnimationFrame();
	AnimationFrame(sf::Sprite sprite, DamagePoint* pDamagePoint);
	AnimationFrame(sf::Sprite sprite, DamagePoint* pDamagePoint, sf::FloatRect collisionBounds);
	sf::Sprite* getSprite();
	DamagePoint* getDamagePoint();
	bool getBreakFrame();
	void setBreakFrame(bool val);
	sf::FloatRect getCollisionBounds();
	void setCollisionBounds(sf::FloatRect value);
};


#endif