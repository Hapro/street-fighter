#include "BlockState.h"
#include "CharacterAnimation.h"
#include "StandState.h"
#include "Character.h"

BlockState::BlockState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pBlockAnimation);
	stateName = "BLOCK_STATE";
}

CharacterState* BlockState::update(float elapsed){
	if (exit){
		return new StandState(pParentCharacter);
	}
	exit = true;
	return this;
}

CharacterState* BlockState::damage(Character* pAttackingCharacter){
	//if we have the same facing as the enemy, then they cannot hurt us
	if (pParentCharacter->getFacing() == pAttackingCharacter->getFacing()){
		return CharacterState::damage(pAttackingCharacter);
	}
	return this;
}

CharacterState* BlockState::block(){
	exit = false;
	return this;
}