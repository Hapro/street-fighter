#ifndef WALK_STATE
#define WALK_STATE
#include "CharacterState.h"

class WalkState : public CharacterState{
private:
	sf::Vector2f direction;
public:
	WalkState(Character* pParnetCharacter, sf::Vector2f direction);
	CharacterState* update(float elapsed) override;
	CharacterState* move(sf::Vector2f direction) override;
	CharacterState* punch() override;
	CharacterState* block() override;
	CharacterState* jump() override;
	CharacterState* crouch() override;
};

#endif