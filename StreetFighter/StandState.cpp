#include "StandState.h"
#include "Character.h"
#include "CharacterAnimation.h"
#include "WalkState.h"
#include "PunchState.h"
#include "DamageState.h"
#include "BlockState.h"
#include "JumpState.h"

StandState::StandState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pStandAnimation);
	stateName = "STAND_STATE";
}

CharacterState* StandState::move(sf::Vector2f direction){
	return new WalkState(pParentCharacter, direction);
}

CharacterState* StandState::punch(){
	return new PunchState(pParentCharacter);
}

CharacterState* StandState::block(){
	return new BlockState(pParentCharacter);
}

CharacterState* StandState::jump(){
	return new JumpState(pParentCharacter);
}

CharacterState* StandState::crouch(){
	return new CrouchState(pParentCharacter);
}

//Crouch state

CrouchState::CrouchState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pCrouchAnimation);
}

CharacterState* CrouchState::punch(){
	return new CrouchPunchState(pParentCharacter);
}

CharacterState* CrouchState::getDamageState(DamagePoint* damagePoint){
	return new FallState(pParentCharacter, damagePoint);
}

CharacterState* CrouchState::crouch(){
	exit = false;
	return this;
}

CharacterState* CrouchState::update(float elapsed){
	if (exit){
		return new StandState(pParentCharacter);
	}
	exit = true;
	return CharacterState::update(elapsed);
}

//Crouch punch state

CrouchPunchState::CrouchPunchState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pCrouchPunchAnimation);
}

CharacterState* CrouchPunchState::getDamageState(DamagePoint* damagePoint){
	return new FallState(pParentCharacter, damagePoint);
}

CharacterState* CrouchPunchState::update(float elapsed){
	if (pAnimation->hasFinished()){
		return new CrouchState(pParentCharacter);
	}
	return CharacterState::update(elapsed);
}