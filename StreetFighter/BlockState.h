#ifndef BLOCK_STATE
#define BLOCK_STATE
#include "CharacterState.h"

class DamagePoint;

class BlockState : public CharacterState{
	bool exit = false;
public:
	BlockState(Character* pCharacterState);
	CharacterState* update(float elapsed) override;
	CharacterState* damage(Character* pAttackingCharacter) override;
	CharacterState* block() override;
};


#endif