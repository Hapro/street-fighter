#include "AnimationFrame.h"
#include "Character.h"

DamagePoint::DamagePoint(int damageToDeal, sf::Vector2f force, sf::Vector2f localPosition){
	this->damageToDeal = damageToDeal;
	this->force = force;
	this->localPosition = localPosition;
	this->active = true;
}

DamagePoint::DamagePoint(){
	active = false;
}

bool DamagePoint::getActive(){
	return active;
}

sf::Vector2f DamagePoint::getForce(){
	return force;
}

sf::Vector2f DamagePoint::getLocalPosition(){
	return localPosition;
}

sf::Vector2f DamagePoint::getFlippedLocalPosition(){
	sf::Vector2f temp = localPosition;
	temp.x = -temp.x;
	return temp;
}

int DamagePoint::getDamageToDeal(){
	return damageToDeal;
}
//*********************************************************

AnimationFrame::~AnimationFrame(){
	delete pDamagePoint;
}

AnimationFrame::AnimationFrame(sf::Sprite sprite, DamagePoint* pDamagePoint){
	this->sprite = sprite;
	this->pDamagePoint = pDamagePoint;
	setCollisionBounds(sprite.getLocalBounds());
}

AnimationFrame::AnimationFrame(sf::Sprite sprite, DamagePoint* pDamagePoint, sf::FloatRect collisionBounds){
	this->sprite = sprite;
	this->pDamagePoint = pDamagePoint;
	setCollisionBounds(collisionBounds);
}

DamagePoint* AnimationFrame::getDamagePoint(){
	return pDamagePoint;
}

sf::Sprite* AnimationFrame::getSprite(){
	return &this->sprite;
}

void AnimationFrame::setBreakFrame(bool val){
	this->breakFrame = val;
}

bool AnimationFrame::getBreakFrame(){
	return breakFrame;
}

sf::FloatRect AnimationFrame::getCollisionBounds(){
	return collisionBounds;
}

void AnimationFrame::setCollisionBounds(sf::FloatRect value){
	collisionBounds = value;
}
