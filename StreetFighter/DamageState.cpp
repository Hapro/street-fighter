#include "DamageState.h"
#include "CharacterAnimation.h"
#include "Character.h"
#include "StandState.h"

DamageState::DamageState(Character* pParentCharacter, DamagePoint* pDamagePoint)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pDamageAnimation);
	this->pDamagePoint = pDamagePoint;
	stateName = "DAMAGE_STATE";
	//take health
}

CharacterState* DamageState::damage(Character* pAttackingCharacter){
	return this;
}

CharacterState* DamageState::getDamageState(DamagePoint* dp){
	return this;
}

CharacterState* DamageState::update(float elapsed){
	//flinch for 0.8s
	if (timeInState >= 0.8f){
		return new StandState(pParentCharacter);
	}
	
	return CharacterState::update(elapsed);
}