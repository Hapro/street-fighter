#ifndef CHARACTER
#define CHARACTER
#include "SFML\Graphics.hpp"

class AnimationFrame;
class CharacterState;
class DamagePoint;
class CharacterAnimation;

//character in the game i.e. a fighter
class Character{
private:
	CharacterState* pCharacterState;
	sf::Vector2f currentLocation;
	bool facing; //0 = left 1 = right
	int health = 50;
	bool onFloor;
	sf::Vector2f currentMovementForce = sf::Vector2f(0, 0);
	float movementAcceleration;
	const float friction = 0.95f;
	const int floorLevel = 500;

	sf::Vector2f currentVelocity = sf::Vector2f(0, 0);
	float movementSpeed = 150;

public:
	CharacterAnimation* pPunchAnimation;
	CharacterAnimation* pKickAnimation;
	CharacterAnimation* pBlockAnimation;
	CharacterAnimation* pDamageAnimation;
	CharacterAnimation* pWalkAnimation;
	CharacterAnimation* pStandAnimation;
	CharacterAnimation* pJumpAnimation;
	CharacterAnimation* pFallAnimation;
	CharacterAnimation* pJumpKickAnimation;
	CharacterAnimation* pCrouchAnimation;
	CharacterAnimation* pOnFloorAnimation;
	CharacterAnimation* pCrouchPunchAnimation;

	Character();
	bool getFacing();
	void setFacing(bool facing);
	void loadAnimations();
	virtual void move(sf::Vector2f direction);
	virtual void jump();
	virtual void punch();
	virtual void kick();
	virtual void block();
	virtual void crouch();
	virtual void damage(Character* attackingCharacter);
	virtual void update(float elapsed);
	virtual void draw(sf::RenderWindow* pWindow);
	void setVelocity(sf::Vector2f velocity);
	float getMovementSpeed();
	sf::Vector2f getVelocity();
	bool getOnFloor();
	AnimationFrame* getCurrentAnimationFrame();
	void setLocation(sf::Vector2f location);
	sf::Vector2f getLocation();
	void deleteStateIfChanged(CharacterState* newState);
	sf::FloatRect getCollisionBounds();
};

#endif