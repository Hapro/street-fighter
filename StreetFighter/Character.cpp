#include "Character.h"
#include "CharacterState.h"
#include "WalkState.h"
#include "StandState.h"
#include "SFML\Graphics.hpp"
#include "AnimationFrame.h"
#include "CharacterAnimation.h"

Character::Character(){
	loadAnimations();
	pCharacterState = new StandState(this);
	this->currentLocation = sf::Vector2f(100, 500);
	facing = true;
}

AnimationFrame* Character::getCurrentAnimationFrame(){
	return pCharacterState->getCurrentAnimationFrame();
}

void Character::setFacing(bool facing){
	this->facing = facing;
}

bool Character::getFacing(){ return facing; }

void Character::damage(Character* pAttackingCharacter){
	CharacterState* newState = pCharacterState->damage(pAttackingCharacter);
	deleteStateIfChanged(newState);
}

void Character::block(){
	CharacterState* newState = pCharacterState->block();
	deleteStateIfChanged(newState);
}

void Character::jump(){
	CharacterState* newState = pCharacterState->jump();
	deleteStateIfChanged(newState);
}

void Character::kick(){
	CharacterState* newState = pCharacterState->punch();

	if (pCharacterState->getStateName() != "PUNCH_STATE" && newState->getStateName() == "PUNCH_STATE"){
		//overite pointer to default punch anim with the kick
		newState->setAnimation(pKickAnimation);
	}
	deleteStateIfChanged(newState);
}

void Character::move(sf::Vector2f direction){
	CharacterState* newState = pCharacterState->move(direction);
	deleteStateIfChanged(newState);
}

void Character::punch(){
	CharacterState* newState = pCharacterState->punch();
	deleteStateIfChanged(newState);
}

void Character::crouch(){
	CharacterState* newState = pCharacterState->crouch();
	deleteStateIfChanged(newState);
}

void Character::update(float elapsed){
	CharacterState* newState = this->pCharacterState->update(elapsed);
	deleteStateIfChanged(newState);

	currentLocation += currentVelocity * elapsed;

	if (!getOnFloor()){
		float diff = floorLevel - (getCollisionBounds().height + getCollisionBounds().top);

		if (diff < 0){
			currentLocation.y += diff;
			currentVelocity.y = 0;
		}
		else{
			currentVelocity.y += 1500 * elapsed; //in the air; apply gravity
		}
	}
	else{
		currentVelocity *= 0.95f; //friction
	}
}

void Character::deleteStateIfChanged(CharacterState* newState){
	//If we've changed state, then delete the old state
	if (newState != pCharacterState){
		delete pCharacterState;
		pCharacterState = newState;
	}
}

sf::Vector2f Character::getLocation(){
	return this->currentLocation;
}

void Character::setLocation(sf::Vector2f location){
	this->currentLocation = location;
}

float Character::getMovementSpeed(){
	return movementSpeed;
}

bool Character::getOnFloor(){
	return (getCollisionBounds().top + getCollisionBounds().height) == floorLevel;
}

void Character::draw(sf::RenderWindow* pWindow){
	this->pCharacterState->draw(pWindow);
}

void Character::setVelocity(sf::Vector2f velocity){
	currentVelocity = velocity;
}

sf::Vector2f Character::getVelocity(){
	return currentVelocity;
}

sf::FloatRect Character::getCollisionBounds(){
	sf::FloatRect rect = pCharacterState->getCurrentAnimationFrame()->getCollisionBounds();

	if (facing){
		rect.left = rect.left + currentLocation.x - rect.width / 2;
	}
	else{
		rect.left = currentLocation.x - (rect.width / 2) - rect.left;
	}

	rect.top += currentLocation.y - rect.height / 2;
	return rect;
}

//ideally we would load from a file, but this beyond my ambition (so far)
void Character::loadAnimations(){
	sf::FloatRect defaultRect = sf::FloatRect(0, 0, 90, 220);

	//punch
	pPunchAnimation = new CharacterAnimation();
	pPunchAnimation->addFrame("kenPunch1", new DamagePoint(), defaultRect);
	pPunchAnimation->addFrame("kenPunch2", new DamagePoint(0, sf::Vector2f(0, 0), sf::Vector2f(115, -55)), defaultRect);
	pPunchAnimation->addFrame("kenPunch3", new DamagePoint(), defaultRect);
	pPunchAnimation->addFrame("kenPunch4", new DamagePoint(), defaultRect);
	pPunchAnimation->addFrame("kenPunch5", new DamagePoint(0, sf::Vector2f(0, 0), sf::Vector2f(140, -65)), defaultRect);
	pPunchAnimation->getFrames().at(2)->setBreakFrame(true);
	pPunchAnimation->reset();

	//kick
	pKickAnimation = new CharacterAnimation();
	pKickAnimation->addFrame("kenKick1", new DamagePoint(), defaultRect);
	pKickAnimation->addFrame("kenKick2", new DamagePoint(0, sf::Vector2f(0, 0), sf::Vector2f(140, -63)), defaultRect);
	pKickAnimation->reset();

	//block
	pBlockAnimation = new CharacterAnimation();
	pBlockAnimation->addFrame("kenBlock1", new DamagePoint(), defaultRect);
	pBlockAnimation->reset();

	//damage
	pDamageAnimation = new CharacterAnimation();
	pDamageAnimation->addFrame("kenHurt1", new DamagePoint(), defaultRect);
	pDamageAnimation->reset();

	//walk
	pWalkAnimation = new CharacterAnimation();
	pWalkAnimation->addFrame("kenWalk1", new DamagePoint(), defaultRect);
	pWalkAnimation->addFrame("kenWalk2", new DamagePoint(), defaultRect);
	pWalkAnimation->addFrame("kenWalk3", new DamagePoint(), defaultRect);
	pWalkAnimation->addFrame("kenWalk4", new DamagePoint(), defaultRect);
	pWalkAnimation->addFrame("kenWalk5", new DamagePoint(), defaultRect);
	pWalkAnimation->addFrame("kenWalk6", new DamagePoint(), defaultRect);
	pWalkAnimation->reset();

	//standing
	pStandAnimation = new CharacterAnimation();
	pStandAnimation->addFrame("kenStand1", new DamagePoint(), defaultRect);
	pStandAnimation->reset();

	//jumping
	pJumpAnimation = new CharacterAnimation();
	pJumpAnimation->addFrame("kenJump1", new DamagePoint(), defaultRect);
	pJumpAnimation->reset();

	//falling
	pFallAnimation = new CharacterAnimation();
	pFallAnimation->addFrame("kenFall1", new DamagePoint(), defaultRect);
	pFallAnimation->reset();

	//jumpKicking
	pJumpKickAnimation = new CharacterAnimation();
	pJumpKickAnimation->addFrame("kenJumpKick1", new DamagePoint(0, sf::Vector2f(0, 0), sf::Vector2f(90, -59)), 
		sf::FloatRect(-20, -20, 100, 100));
	pJumpKickAnimation->reset();

	//on floor
	pOnFloorAnimation = new CharacterAnimation();
	pOnFloorAnimation->addFrame("kenFloor1", new DamagePoint(), sf::FloatRect(0, 0, 252, 67));
	pOnFloorAnimation->reset();

	//crouching
	pCrouchAnimation = new CharacterAnimation();
	pCrouchAnimation->addFrame("kenCrouch1", new DamagePoint(), sf::FloatRect(-14, 53, 105, 136));
	pCrouchAnimation->reset();

	//crouch punch
	pCrouchPunchAnimation = new CharacterAnimation();
	pCrouchPunchAnimation->addFrame("kenCrouchPunch1", new DamagePoint(), sf::FloatRect(-14, 53, 105, 136));
	pCrouchPunchAnimation->addFrame("kenCrouchPunch2", new DamagePoint(0, sf::Vector2f(0, 0), sf::Vector2f(123, -20)),
		sf::FloatRect(-14, 53, 105, 136));
	pCrouchPunchAnimation->reset();
}