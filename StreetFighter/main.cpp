#include <SFML/Graphics.hpp>
#include "Game.h"

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Green);
	Game game = Game();
	sf::Clock gameClock = sf::Clock();

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		game.update(gameClock.getElapsedTime().asSeconds());
		gameClock.restart();

		window.clear();
		game.draw(&window);
		window.display();
	}

	return 0;
}