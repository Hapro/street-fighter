#include "CharacterState.h"
#include "CharacterAnimation.h"
#include "Character.h"
#include "WalkState.h"
#include "DamageState.h"

CharacterState::~CharacterState(){
}

CharacterState::CharacterState(Character* pParentCharacter){
	timeInState = 0.0f;
	this->pParentCharacter = pParentCharacter;
	font = sf::Font();
	font.loadFromFile("Vera.ttf");
	stateName = "DEFAULT";
}

CharacterState* CharacterState::block(){
	return this;
}

CharacterState* CharacterState::jump(){
	return this;
}

CharacterState* CharacterState::kick(){
	return this;
}

CharacterState* CharacterState::move(sf::Vector2f direction){
	return this;
}

CharacterState* CharacterState::punch(){
	return this;
}

CharacterState* CharacterState::crouch(){
	return this;
}

CharacterState* CharacterState::update(float elapsed){
	sf::Keyboard keys = sf::Keyboard();
	timeInState += elapsed;
	pAnimation->update(timeInState);
	return this;
}

void CharacterState::setAnimation(CharacterAnimation* animation){
	this->pAnimation = animation;
	pAnimation->reset(); //refactor out somewhere else
}

void CharacterState::draw(sf::RenderWindow* pWindow){
	sf::Sprite* sprite = this->pAnimation->getCurrentFrame()->getSprite();
	sprite->setPosition(pParentCharacter->getLocation());

	//0 = left facing
	if (!pParentCharacter->getFacing()){
		sprite->setScale(-1.0f, 1.0f);
	}
	else{
		sprite->setScale(1.0f, 1.0f);
	}

	pWindow->draw(*sprite);
}

AnimationFrame* CharacterState::getCurrentAnimationFrame(){
	return this->pAnimation->getCurrentFrame();
}

CharacterState* CharacterState::damage(Character* pAttackingCharacter){
	DamagePoint* dp = pAttackingCharacter->getCurrentAnimationFrame()->getDamagePoint();

	if (dp->getActive() && checkDamagePointCollision(pAttackingCharacter, dp)){
		return getDamageState(dp);
	}

	return this;
}

CharacterState* CharacterState::getDamageState(DamagePoint* dp){
	return new DamageState(pParentCharacter, dp);
}

bool CharacterState::checkDamagePointCollision(Character* pAttackingCharacter, DamagePoint* pDamagePoint){
	sf::Vector2f loc = pAttackingCharacter->getLocation();
	sf::FloatRect rect = pParentCharacter->getCollisionBounds();

	if (!pAttackingCharacter->getFacing()){
		loc += pDamagePoint->getFlippedLocalPosition();
	}
	else{
		loc += pDamagePoint->getLocalPosition();
	}

	if (rect.contains(loc.x, loc.y)) return true;

	return false;
}

std::string CharacterState::getStateName(){
	return stateName;
}