#ifndef STAND_STATE
#define STAND_STATE
#include "CharacterState.h"

class StandState : public CharacterState{
public:
	StandState(Character* pParentCharacter);
	CharacterState* punch() override;
	CharacterState* move(sf::Vector2f direction) override;
	CharacterState* block() override;
	CharacterState* jump() override;
	CharacterState* crouch() override;
};

class CrouchState : public CharacterState{
private:
	bool exit = false;
public:
	CrouchState(Character* pParentCharacter);
	CharacterState* punch() override;
	CharacterState* getDamageState(DamagePoint* damagePoint) override;
	CharacterState* crouch() override;
	CharacterState* update(float elapsed) override;
};

class CrouchPunchState : public CharacterState{
public:
	CrouchPunchState(Character* pParentCharacter);
	CharacterState* getDamageState(DamagePoint* damagePoint) override;
	CharacterState* update(float elapsed) override;
};

#endif