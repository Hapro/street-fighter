#ifndef JUMP_STATE
#define JUMP_STATE
#include "CharacterState.h"
#include "DamageState.h"
//Jump and fall state in here

class Character;

class JumpState : public CharacterState{
public:
	JumpState(Character* pParentCharacter);
	CharacterState* getDamageState(DamagePoint* dp) override;
	CharacterState* punch() override;
	CharacterState* kick() override;
	CharacterState* update(float elapsed) override;
};

class JumpKickState : public CharacterState{
public:
	JumpKickState(Character* pParentCharacter);
	CharacterState* update(float elapsed) override;
	CharacterState* getDamageState(DamagePoint* dp) override;
};

class FallState : public DamageState{
private:
	float timeOnFloor;

public:
	FallState(Character* pParentCharacter, DamagePoint* dp);
	CharacterState* update(float elapsed) override;
};


#endif