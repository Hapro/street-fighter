#ifndef CHARACTER_ANIMATION
#define CHARACTER_ANIMATION
#include <SFML\Graphics.hpp>
#include <vector>
#include "AnimationFrame.h"

class Character;

class CharacterAnimation{
private:
	std::vector<AnimationFrame*> frames;
	const float framerate = 5.0f;
	bool looped;
	bool ignoreBreakFrame;
	AnimationFrame* currentFrame;
	AnimationFrame* previousFrame; //what frame was like last time update was called

public:
	~CharacterAnimation();
	CharacterAnimation(std::string filepath, int noOfFrames,
		std::vector<DamagePoint*> damagePointData);
	CharacterAnimation();

	void loadFrames(std::string filepath, int noOfFrames,
		std::vector<DamagePoint*> damagePointData);

	//adds a frame to anim by loading a new one from filename provided
	void addFrame(std::string fileName, DamagePoint* damagePointData, sf::FloatRect collisionBounds);

	std::vector<AnimationFrame*> getFrames();

	void update(float timeElapsed);

	AnimationFrame* getCurrentFrame();
	AnimationFrame* getPreviousFrame();
	bool hasFinished();
	bool getLooped();
	bool getAtBreakFrame();
	void setIgnoreBreakFrame(bool value);
	void reset();
};

#endif