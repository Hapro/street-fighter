#include "Game.h"
#include "Character.h"
#include "CharacterState.h"
#include "CharacterAnimation.h"
#include "SFML\Graphics.hpp"

Game::Game(){
	sf::Texture* img = new sf::Texture();
	img->loadFromFile("StreetFighterImages/background.png");
	pPlayer = new Character();
	pTestEnemy = new Character();
	background = sf::Sprite(*img);
	background.setPosition(sf::Vector2f(0, 0));
}

void Game::update(float elasped){
	sf::Keyboard keys;

	if (keys.isKeyPressed(sf::Keyboard::A)){
		pPlayer->move(sf::Vector2f(-1, 0));
	}
	if (keys.isKeyPressed(sf::Keyboard::D)){
		pPlayer->move(sf::Vector2f(1, 0));
	}
	if (keys.isKeyPressed(sf::Keyboard::K)){
		pPlayer->punch();
	}
	if (keys.isKeyPressed(sf::Keyboard::LShift)){
		pPlayer->block();
	}
	if (keys.isKeyPressed(sf::Keyboard::L)){
		pPlayer->kick();
	}
	if (keys.isKeyPressed(sf::Keyboard::Space)){
		pPlayer->jump();
	}
	if (keys.isKeyPressed(sf::Keyboard::LControl)){
		pPlayer->crouch();
	}
	pTestEnemy->punch();

	pPlayer->update(elasped);
	pTestEnemy->update(elasped);

	pTestEnemy->damage(pPlayer);
	pPlayer->damage(pTestEnemy);

}

void Game::draw(sf::RenderWindow* window){
	window->draw(background);
	pPlayer->draw(window);
	pTestEnemy->draw(window);

	sf::RectangleShape bounds = sf::RectangleShape();
	sf::FloatRect playerBounds = pPlayer->getCollisionBounds();
	sf::Color color = sf::Color::Blue;
	color.a = 100;

	bounds.setSize(sf::Vector2f(playerBounds.width,
		playerBounds.height));
	bounds.setPosition(playerBounds.left, playerBounds.top);
	bounds.setFillColor(color);

	window->draw(bounds);

	if (pPlayer->getCurrentAnimationFrame()->getDamagePoint()->getActive()){
		sf::RectangleShape hitLoc = sf::RectangleShape(sf::Vector2f(10, 10));
		hitLoc.setFillColor(sf::Color::Red);

		if (pPlayer->getFacing()){
			hitLoc.setPosition(pPlayer->getCurrentAnimationFrame()->getDamagePoint()->getLocalPosition() +
				pPlayer->getLocation());
		}
		else{
			hitLoc.setPosition(pPlayer->getCurrentAnimationFrame()->getDamagePoint()->getFlippedLocalPosition() +
				pPlayer->getLocation());
		}
		hitLoc.setOrigin(5, 5);
		window->draw(hitLoc);
	}
}