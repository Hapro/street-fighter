#ifndef CHARACTER_STATE
#define CHARACTER_STATE
#include "SFML\Graphics.hpp"

class CharacterAnimation;
class Character;
class AnimationFrame;
class DamagePoint;

//This is the base state for our FSM. Actions reflect what is
//possible for each character
class CharacterState{
protected:
	CharacterAnimation* pAnimation;
	float timeInState = 0;
	Character* pParentCharacter;
	sf::Font font;
	std::string stateName;

public:
	~CharacterState();
	CharacterState(Character* pParentCharacter);
	virtual CharacterState* update(float elapsed);
	//essentially a list of actions allowed by this state. If disallowed, 
	//we simply return the state we are in. Otherwise we return the correct state
	virtual CharacterState* move(sf::Vector2f direction);
	virtual CharacterState* jump();
	virtual CharacterState* punch();
	virtual CharacterState* kick();
	virtual CharacterState* block();
	virtual CharacterState* crouch();
	virtual CharacterState* getDamageState(DamagePoint* dp); //some states might have a differing damage state to return to
	virtual void setAnimation(CharacterAnimation* animation);
	//default is to hurt the player. immune states must be explicitly specified
	virtual CharacterState* damage(Character* pAttackingCharacter);
	virtual void draw(sf::RenderWindow* pWindow);
	AnimationFrame* getCurrentAnimationFrame();
	//checks if this character is hurting us
	bool checkDamagePointCollision(Character* pAttackingCharacter, DamagePoint* pDamagePoint);
	std::string getStateName();
};

#endif