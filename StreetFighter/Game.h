#ifndef GAME
#define GAME
#include "SFML\Graphics.hpp"

class RenderWindow;
class Character;

class Game{
private:
	Character* pPlayer;
	Character* pTestEnemy;
	sf::Sprite background;
public:
	Game();
	void update(float elapsed);
	void draw(sf::RenderWindow* window);
};

#endif