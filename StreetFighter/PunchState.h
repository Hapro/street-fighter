#ifndef PUNCH_STATE
#define PUNCH_STATE
#include "CharacterState.h"

class PunchState : public CharacterState{
public:
	PunchState(Character* pParentCharacter);
	CharacterState* update(float elapsed) override;
	CharacterState* punch() override;
};

#endif