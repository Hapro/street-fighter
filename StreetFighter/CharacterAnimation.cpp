#include "CharacterAnimation.h"
#include "AnimationFrame.h"

CharacterAnimation::~CharacterAnimation(){
	for (int x = 0; x < frames.size(); x++){
		delete frames.at(x);
	}
}

CharacterAnimation::CharacterAnimation(){
}

CharacterAnimation::CharacterAnimation(std::string filepath, int noOfFrames, 
	std::vector<DamagePoint*> damagePointData){
	loadFrames(filepath, noOfFrames, damagePointData);
	reset();
}

std::vector<AnimationFrame*> CharacterAnimation::getFrames(){
	return frames;
}

bool CharacterAnimation::hasFinished(){
	return looped || (previousFrame->getBreakFrame() && !ignoreBreakFrame);
}

//adds a frame to anim by loading a new one from filename provided
void CharacterAnimation::addFrame(std::string fileName, DamagePoint* damagePointData, sf::FloatRect collisionBounds){
	fileName = "StreetFighterImages/" + fileName;

	sf::Texture* tex = new sf::Texture();
	sf::Sprite sprite;
	tex->loadFromFile(fileName + ".png");
	sprite = sf::Sprite(*tex);

	//Set origin to center of sprite
	sprite.setOrigin(sprite.getLocalBounds().width / 2,
		sprite.getLocalBounds().height / 2);

	this->frames.push_back(new AnimationFrame(sprite, damagePointData, collisionBounds));
}

void CharacterAnimation::loadFrames(std::string filepath, int noOfFrames,
	std::vector<DamagePoint*> damagePointData){

	filepath = "StreetFighterImages/" + filepath;

	for (int x = 1; x <= noOfFrames; x++){
		sf::Texture* tex = new sf::Texture();
		sf::Sprite sprite;
		std::string number = std::to_string(x);

		tex->loadFromFile(filepath + number + ".png");
		sprite = sf::Sprite(*tex);
		//Set origin to center of sprite
		sprite.setOrigin(sprite.getLocalBounds().width / 2,
			sprite.getLocalBounds().height / 2);

		this->frames.push_back(new AnimationFrame(sprite, damagePointData.at(x - 1)));
	}
}

void CharacterAnimation::update(float timeElapsed){
	int frame = floor(framerate * timeElapsed);

	if (frame >= frames.size()){
		looped = true;
		frame = frame % frames.size();
	}
	//When the frame changes, we refresh the ignoreBreakFrame flag
	if (currentFrame != previousFrame){ ignoreBreakFrame = false; }

	previousFrame = currentFrame;
	this->currentFrame = frames.at(frame);
}

AnimationFrame* CharacterAnimation::getCurrentFrame(){
	return currentFrame;
}

AnimationFrame* CharacterAnimation::getPreviousFrame(){
	return previousFrame;
}

//returns true if the animation has been played the whole way through at least once
bool CharacterAnimation::getLooped(){
	return looped;
}

bool CharacterAnimation::getAtBreakFrame(){
	return currentFrame->getBreakFrame();
}

void CharacterAnimation::reset(){
	looped = false;
	currentFrame = frames.at(0);
	previousFrame = currentFrame;
}

void CharacterAnimation::setIgnoreBreakFrame(bool value){
	ignoreBreakFrame = value;
}