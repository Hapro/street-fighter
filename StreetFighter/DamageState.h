#ifndef DAMAGE_STATE
#define DAMAGE_STATE
#include "CharacterState.h"
#include "AnimationFrame.h"

class DamageState : public CharacterState{
	DamagePoint* pDamagePoint;
public:
	DamageState(Character* pParentCharacter, DamagePoint* damagePoint);
	CharacterState* update(float elapsed) override;
	CharacterState* damage(Character* pAttackingCharacter) override;
	CharacterState* getDamageState(DamagePoint* dp) override;
};

#endif