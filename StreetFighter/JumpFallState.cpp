#include "JumpState.h"
#include "Character.h"
#include "StandState.h"

JumpState::JumpState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pJumpAnimation);
	pParentCharacter->setVelocity(pParentCharacter->getVelocity() + sf::Vector2f(0, -400));
	stateName = "JUMP_STATE";
}

CharacterState* JumpState::punch(){
	return new JumpKickState(pParentCharacter);
}
CharacterState* JumpState::kick(){
	return new JumpKickState(pParentCharacter);
}

CharacterState* JumpState::update(float elapsed){
	if (pParentCharacter->getOnFloor()){
		return new StandState(pParentCharacter);
	}
	
	return CharacterState::update(elapsed);
}

CharacterState* JumpState::getDamageState(DamagePoint* dp){
	return new FallState(pParentCharacter, dp);
}

//JumpKickState***********************

JumpKickState::JumpKickState(Character* pParentCharacter)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pJumpKickAnimation);
	stateName = "JUMP_KICK_STATE";
}

CharacterState* JumpKickState::getDamageState(DamagePoint* dp){
	return new FallState(pParentCharacter, dp);
}

CharacterState* JumpKickState::update(float elapsed){
	if (pParentCharacter->getOnFloor()){
		return new StandState(pParentCharacter);
	}

	return CharacterState::update(elapsed);
}

//Fallstate**********************

FallState::FallState(Character* pParentCharacter, DamagePoint* dp)
	:DamageState(pParentCharacter, dp){
	timeOnFloor = 0.0f;
	setAnimation(pParentCharacter->pFallAnimation);
	stateName = "FALL_STATE";
}

CharacterState* FallState::update(float elapsed){
	if (pParentCharacter->getOnFloor()){
		if (timeOnFloor == 0){
			setAnimation(pParentCharacter->pOnFloorAnimation);
		}
		//get up after .3 seconds
		if (timeOnFloor > 0.3f){
			return new StandState(pParentCharacter);
		}
		timeOnFloor += elapsed;
	}
	else{
		timeOnFloor = 0.0f;
	}
	
	return CharacterState::update(elapsed);
}