#include "WalkState.h"
#include "Character.h"
#include "PunchState.h"
#include "CharacterAnimation.h"
#include "StandState.h"
#include "DamageState.h"
#include "BlockState.h"
#include "JumpState.h"

WalkState::WalkState(Character* pParentCharacter, sf::Vector2f direction)
	:CharacterState(pParentCharacter){
	setAnimation(pParentCharacter->pWalkAnimation);
	this->direction = direction;
	stateName = "WALK_STATE";
}

CharacterState* WalkState::update(float elapsed){
	if (direction.x == 0){ return new StandState(pParentCharacter); }
	if (direction.x > 0) pParentCharacter->setFacing(true);
	if (direction.x < 0) pParentCharacter->setFacing(false);

	//Move the character by the right speed
	pParentCharacter->setVelocity(
		direction * 
		pParentCharacter->getMovementSpeed());

	this->direction = sf::Vector2f(0, 0);

	return CharacterState::update(elapsed);
}

//Pass a direction vector of 0 to stop moving
CharacterState* WalkState::move(sf::Vector2f direction){
	this->direction = direction;
	return this;
}

CharacterState* WalkState::punch(){
	return new PunchState(pParentCharacter);
}

CharacterState* WalkState::block(){
	return new BlockState(pParentCharacter);
}

CharacterState* WalkState::jump(){
	return new JumpState(pParentCharacter);
}

CharacterState* WalkState::crouch(){
	return new CrouchState(pParentCharacter);
}